import { GraphQLServer } from 'graphql-yoga';
import { request } from 'graphql-request';
import { resolvers, prisma } from './api';
import bodyParser from 'body-parser';
import cors from 'cors';

const server = new GraphQLServer({
  typeDefs: './db/schema.graphql',
  resolvers,
  context: context => ({
    ...context,
    prisma
  })
});

server.express.use(cors());
server.express.use(bodyParser.json());
server.express
  .get('/verifyUserEmail', async (req, res) => {
    try {
      const { token } = req.query;
      const query =
      `mutation {
        verifyUserEmail(token: "${token}") {
          id
          token
          userName
        }
      }`;

      const response = await request(`http://localhost:${process.env.API_PORT}/`, query);
      res.json(response);
    } catch (err) {
      res.json(err);
      throw new Error(err.message || '#ERR_FFFF');
    }
  })
  .get('/routeList', async (req, res) => {
    try {
      const query =
      `query {
        routes {
          id
          description
          head
          name
          path
          meta
          layout {
            id
            name
            component
          }
        }
      }`;

      const response = await request(`http://localhost:${process.env.API_PORT}/`, query);
      res.json(response);
    } catch (err) {
      res.json(err);
      throw new Error(err.message || '#ERR_FFFF');
    }
  });

server.start({ port: process.env.API_PORT }, () => console.log(`Server is running on http://localhost:${process.env.API_PORT}`));
