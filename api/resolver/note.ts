import { prisma } from '../model';

const Note = {
  targetQuote(parent) {
    return prisma.note({ id: parent.id }).targetQuote();
  },

  user(parent) {
    return prisma.note({ id: parent.id }).user();
  }
};

export default Note;
