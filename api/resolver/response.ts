import { prisma } from '../model';

const Response = {
  responses(parent) {
    return prisma.response({ id: parent.id }).responses();
  },

  targetPost(parent) {
    return prisma.response({ id: parent.id }).targetPost();
  },

  user(parent) {
    return prisma.response({ id: parent.id }).user();
  }
};

export default Response;
