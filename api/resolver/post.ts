import { prisma } from '../model';

const Post = {
  author(parent) {
    return prisma.post({ id: parent.id }).author();
  },

  paragraphs(parent) {
    return prisma.post({ id: parent.id }).paragraphs({ orderBy: 'index_ASC' });
  },

  tags(parent) {
    return prisma.post({ id: parent.id }).tags();
  },

  responses(parent) {
    return prisma.post({ id: parent.id }).responses();
  }
};

export default Post;
