import { prisma } from '../model';

const Creator = {
  items(parent) {
    return prisma.creator({ id: parent.id }).items();
  },

  paragraphs(parent) {
    return prisma.category({ id: parent.id }).paragraphs({ orderBy: 'index_ASC' });
  }
};

export default Creator;
