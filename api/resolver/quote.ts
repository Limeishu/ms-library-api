import { prisma } from '../model';

const Quote = {
  notes(parent) {
    return prisma.quote({ id: parent.id }).notes();
  },

  targetParagraph(parent) {
    return prisma.quote({ id: parent.id }).targetParagraph();
  },

  user(parent) {
    return prisma.quote({ id: parent.id }).user();
  }
};

export default Quote;
