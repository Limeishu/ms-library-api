import { prisma } from '../model';

const Paragraph = {
  quotes(parent) {
    return prisma.paragraph({ id: parent.id }).quotes();
  },

  responses(parent) {
    return prisma.paragraph({ id: parent.id }).responses();
  }
};

export default Paragraph;
