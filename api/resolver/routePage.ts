import { prisma } from '../model';

const RoutePage = {
  layout(parent) {
    return prisma.routePage({ id: parent.id }).layout();
  },

  targetCategories(parent) {
    return prisma.routePage({ id: parent.id }).targetCategories();
  },

  targetTags(parent) {
    return prisma.routePage({ id: parent.id }).targetTags();
  }
};

export default RoutePage;
