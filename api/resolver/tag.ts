import { prisma } from '../model';

const Tag = {
  posts(parent) {
    return prisma.tag({ id: parent.id }).posts();
  }
};

export default Tag;
