import { prisma } from '../model';

const Category = {
  items(parent) {
    return prisma.category({ id: parent.id }).items();
  },

  paragraphs(parent) {
    return prisma.category({ id: parent.id }).paragraphs({ orderBy: 'index_ASC' });
  }
};

export default Category;
