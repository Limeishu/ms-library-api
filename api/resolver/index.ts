import Category from './category';
import Creator from './creator';
import Item from './item';
import Note from './note';
import Paragraph from './paragraph';
import Post from './post';
import Quote from './quote';
import Response from './response';
import RoutePage from './routePage';
import Tag from './tag';
import User from './user';

export default {
  Category,
  Creator,
  Item,
  Note,
  Paragraph,
  Post,
  Quote,
  Response,
  RoutePage,
  Tag,
  User
};
