import { prisma } from '../model';

const User = {
  group(parent) {
    return prisma.user({ id: parent.id }).group();
  }
};

export default User;
