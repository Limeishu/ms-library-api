import { prisma } from '../model';

const Item = {
  categories(parent) {
    return prisma.item({ id: parent.id }).categories();
  },

  creators(parent) {
    return prisma.item({ id: parent.id }).creators();
  },

  paragraphs(parent) {
    return prisma.category({ id: parent.id }).paragraphs({ orderBy: 'index_ASC' });
  }
};

export default Item;
