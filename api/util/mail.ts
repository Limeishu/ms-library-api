import { createTransport, SentMessageInfo } from 'nodemailer';
import * as dotenv from 'dotenv';

dotenv.config();

const transporter = createTransport({
  service: process.env.SERVICE_TYPE,
  auth: {
    user: process.env.SERVICE_MAIL_USER,
    pass: process.env.SERVICE_MAIL_PASSWORD
  }
});

export interface MailOptions {
  from: string;
  to: string;
  subject: string;
  text: string;
  html: string;
}

export class Mail {
  private options: MailOptions = {
    from: '',
    to: '',
    subject: '',
    text: '',
    html: ''
  };

  constructor(to: string, subject: string, text: string, html: string) {
    this.options.from = process.env.SERVICE_MAIL_USER;
    this.options.to = to;
    this.options.subject = subject;
    this.options.text = text;
    this.options.html = html;
  }

  public async send(): Promise<SentMessageInfo> {
    return await transporter.sendMail(this.options);
  }
}
