import jwt from 'jsonwebtoken';
import path from 'path';
import fs from 'fs';
import { Context } from 'graphql-yoga/dist/types';
import { TokenPayload, UserPayload, GroupPayload } from '../types';
import * as dotenv from 'dotenv';

dotenv.config();

const publicKey = fs.readFileSync(path.resolve(__dirname, '../../', process.env.PUBLIC_KEY));

const tokenParser = async (context: Context, originToken?: string): Promise<TokenPayload> => {
  try {
    if (!originToken && !context.request.get('Authorization')) { throw new Error('#ERR_F000'); }

    const token = originToken || context.request.get('Authorization').split('Bearer ')[1];

    const userId = await jwt.verify(token, publicKey, { algorithm: 'RS256' }, (err, payload) => {
      if (err) { throw new Error('#ERR_U0FF'); }
      return payload.id;
    });

    if (!userId) { throw new Error('#ERR_U0FF'); }

    const targetUser: UserPayload = await context.prisma.user({ id: userId });

    if (!targetUser) { throw new Error('#ERR_U00F'); }

    const userGroup: GroupPayload = await context.prisma.user({ id: userId }).group();

    if (userGroup.name === 'Banned') { throw new Error('#ERR_UFFF'); }

    return {
      user: targetUser,
      group: userGroup
    };
  } catch (err) {
    throw new Error(err.message || '#ERR_FFFF');
  }
};

export default tokenParser;
