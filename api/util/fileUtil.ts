import { createWriteStream } from 'fs';
import { prisma } from '../model';
import path from 'path';

const uploadPath = path.resolve(__dirname, '../../', process.env.UPLOAD_PATH);

interface PathPayload {
  id: string;
  path: string;
}

const FileUpload = async (file, userId): Promise<PathPayload> => {
  const { createReadStream, filename, mimetype } = await file;

  if (!/image/i.test(mimetype)) { throw new Error('#ERR_F000'); }

  const { id, path } = await generatePath(filename, userId);

  return new Promise((resolve, reject) => {
    createReadStream()
      .pipe(createWriteStream(`${uploadPath}/${path}`))
      .on('finish', () => resolve({ id, path }))
      .on('error', reject);
  });
};

const generatePath = async (filename: string, userId): Promise<PathPayload> => {
  const { id } = await prisma.createImage({
    name: filename,
    path: '',
    user: {
      connect: {
        id: userId
      }
    }
  });

  return {
    id,
    path: `${id}-${filename}`
  };
};

export default FileUpload;
