import h2p from 'html2plaintext';
import shorter from 'shorter';
import heml from 'heml';
import fs from 'fs';
import path from 'path';

const registerTemp = fs.readFileSync(path.resolve(__dirname, './register.heml'));

interface MailTemplateType {
  subject: string;
  text: string;
  html: string;
}

const mailTemplate = async (userName: string, token: string): Promise<MailTemplateType> => {
  const rawText = await htmlTemplate(userName, Buffer.from(shorter.compress(token), 'ascii').toString('base64'));
  return {
    subject: '[梅樹文庫] 帳號驗證',
    text: h2p(rawText),
    html: rawText
  };
};

const htmlTemplate = async (userName: string, token: string): Promise<string> => {
  const text = await heml(registerTemp);
  text.html = text.html.replace('{{ $username }}', userName);
  text.html = text.html.replace('{{ $token }}', token);

  return text.html;
};

export default mailTemplate;
