import {
  ID_Input,
  Json,
  Note,
  Paragraph,
  ParagraphWhereUniqueInput,
  QuoteCreateManyWithoutTargetParagraphInput,
  ResponseCreateManyInput,
  UUID
} from './model';

/**
 * Types
 */

export declare type MarkupType =
  | 'EMBED'
  | 'ITEM'
  | 'IMAGE'
  | 'LINK'
  | 'ITALIC'
  | 'STRONG';

export declare type ParagraphType =
  | 'HEADING'
  | 'SUBHEADING'
  | 'QUOTE'
  | 'PART'
  | 'PARAGRAPH'
  | 'IMAGE';

export declare type QuoteType =
  | 'HIGHLIGHT'
  | 'NOTE'
  | 'RESPONSE';

/**
 * Payloads
 */

export declare type GeneralPayload = {
  status?: string;
  message?: string;
  meta?: Json;
};

export declare type ImagePayload = {
  id?: string;
  description?: string;
  name?: string;
  path: string;
  user?: UserPayload;
  meta?: Json;
};

export declare type GroupPayload = {
  accessbility: PermissionClass;
  description: string;
  name: string;
  meta: Json;
};

export declare type LoginPayload = UserPayload & {
  group: GroupPayload;
};

export declare type MarkupPayload = {
  type: MarkupType;
  start?: number;
  end?: number;
  href?: string;
  rel?: string;
  src?: string;
  title?: string;
  meta?: Json;
};

export declare type ParagraphCreateInputPayload = {
  index?: number;
  text?: string;
  meta?: Json;
  quotes?: QuoteCreateManyWithoutTargetParagraphInput;
  responses?: ResponseCreateManyInput;
  type: ParagraphType;
  markup?: {
    set: MarkupPayload[] | MarkupPayload;
  };
};

export declare type ParagraphPayload = {
  create: ParagraphCreateInputPayload[] | ParagraphCreateInputPayload;
  delete: ParagraphWhereUniqueInput[] | ParagraphWhereUniqueInput;
};

export declare type PermissionPayload = {
  read: boolean;
  write: boolean;
  delete: boolean;
};

export declare type PermissionClass = {
  category: PermissionPayload;
  commit: PermissionPayload;
  creator: PermissionPayload;
  group: PermissionPayload;
  item: PermissionPayload;
  image: PermissionPayload;
  post: PermissionPayload;
  user: PermissionPayload;
};

export declare type QuotePayload = {
  id: ID_Input;
  startOffset: number;
  endOffset: number;
  type: QuoteType;
  notes?: Note[];
  targetParagraph: Paragraph;
  user: UserPayload;
  meta?: Json;
};

export declare type TokenPayload = {
  group: GroupPayload;
  user: UserPayload;
};

export declare type UserPayload = {
  id: UUID;
  token: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  meta?: Json
};
