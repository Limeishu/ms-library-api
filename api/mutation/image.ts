import { Context } from 'graphql-yoga/dist/types';
import tokenParser from '../util/tokenParser';
import FileUpload from '../util/fileUtil';
import { ImagePayload } from '../types';

const imageMutation = {
  async uploadImage(_, { file }, context: Context): Promise<ImagePayload> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.image.write) { throw new Error('#ERR_F000'); }

      const { id, path } = await FileUpload(file, tokenPayload.user.id);

      return await context.prisma.updateImage({
        data: {
          path
        },
        where: {
          id
        }
      });

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default imageMutation;

