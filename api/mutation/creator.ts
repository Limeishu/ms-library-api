import { Context } from 'graphql-yoga/dist/types';
import tokenParser from '../util/tokenParser';
import { Creator } from '../model';
import { GeneralPayload, ParagraphPayload } from '../types';

const creatorMutation = {
  async createCreator(_, args, context: Context): Promise<Creator> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.creator.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.createCreator({
        name: args.name,
        description: args.description,
        content: args.content,
        paragraphs: paragraphPayload,
        date: args.date
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateCreator(_, args, context: Context): Promise<Creator> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.creator.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.updateCreator({
        data: {
          name: args.name,
          description: args.description,
          content: args.content,
          paragraphs: paragraphPayload,
          date: args.date,
          meta: args.meta
        },
        where: {
          id: args.id
        }
      });
    } catch (err) {
      if (/No Node for the model/i.test(err.message)) { throw new Error('#ERR_Q001'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deleteCreator(_, { id }, context: Context): Promise<GeneralPayload> {
    try {
      const creatorAuthor = await context.prisma.creator({ id }).author();

      if (!creatorAuthor) { throw new Error('#ERR_A001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.creator.delete || tokenPayload.user.id !== creatorAuthor.id) { throw new Error('#ERR_F000'); }

      await context.prisma.deleteCreator({ id });

      return {
        status: 'ok'
      };

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default creatorMutation;
