import { Context } from 'graphql-yoga/dist/types';
import tokenParser from '../util/tokenParser';
import { Post, Tag } from '../model';
import { GeneralPayload, ParagraphPayload } from '../types';

const postMutation = {
  async createPost(_, args, context: Context): Promise<Post> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.post.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.createPost({
        title: args.title,
        content: args.content,
        paragraphs: paragraphPayload,
        author: { connect: { id: tokenPayload.user.id } }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updatePost(_, args, context: Context): Promise<Post> {
    try {
      const postAuthor = await context.prisma.post({ id: args.id }).author();

      if (!postAuthor) { throw new Error('#ERR_P001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.post.write || tokenPayload.user.id !== postAuthor.id) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.updatePost({
        data: {
          title: args.title,
          content: args.content,
          paragraphs: paragraphPayload,
          published: args.published,
          meta: args.meta
        },
        where: {
          id: args.id
        }
      });
    } catch (err) {
      if (/No Node for the model/i.test(err.message)) { throw new Error('#ERR_Q001'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async setTagsToPost(_, { id, tags }, context: Context): Promise<Post> {
    try {
      const targetPost = await context.prisma.post({ id });

      if (!targetPost) { throw new Error('#ERR_P001'); }

      const postAuthor = await context.prisma.post({ id }).author();
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.post.write || tokenPayload.user.id !== postAuthor.id) { throw new Error('#ERR_F000'); }

      const postExTags = await context.prisma.post({ id }).tags();
      const postExTagsString: string[] = postExTags.map(ele => ele.name);

      const tagsExist: Tag[] = await context.prisma.tags();
      const tagsExistString: string[] = tagsExist.map(tag => tag.name);
      const tagsNotNeedToCreate: string[] = tags.filter(ele => tagsExistString.indexOf(ele) > -1);
      const tagsNeedToCreate: string[] = tags.filter(ele => tagsExistString.indexOf(ele) === -1);
      const tagsNeedToRemove: string[] = postExTagsString.filter(ele => tags.indexOf(ele) === -1);

      return await context.prisma.updatePost({
        data: {
          tags: {
            connect: tagsNotNeedToCreate.map(ele => ({name: ele})),
            disconnect: tagsNeedToRemove.map(ele => ({name: ele})),
            create: tagsNeedToCreate.map(ele => ({name: ele}))
          }
        },
        where: {
          id
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deletePost(_, { id }, context: Context): Promise<GeneralPayload> {
    try {
      const postAuthor = await context.prisma.post({ id }).author();

      if (!postAuthor) { throw new Error('#ERR_P001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.post.delete || tokenPayload.user.id !== postAuthor.id) { throw new Error('#ERR_F000'); }

      await context.prisma.deletePost({ id });

      return {
        status: 'ok'
      };

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default postMutation;
