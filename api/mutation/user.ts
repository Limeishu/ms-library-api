import { Context } from 'graphql-yoga/dist/types';
import { LoginPayload, UserPayload } from '../types';
import bcrypt from 'bcryptjs';
import fs from 'fs';
import jwt from 'jsonwebtoken';
import { Mail } from '../util/mail';
import mailTemplate from '../template/mail';
import path from 'path';
import shorter from 'shorter';
import tokenParser from '../util/tokenParser';

const privateKey = fs.readFileSync(path.resolve(__dirname, '../../', process.env.PRIVATE_KEY));

const userMutation = {
  async signUpUser(_, args, context: Context): Promise<UserPayload> {
    try {
      const userExist = await context.prisma.user({ email: args.email });

      if (userExist) { throw new Error('#ERR_U000'); }

      const password = await bcrypt.hash(args.password, 10);
      const user = await context.prisma.createUser({
        userName: args.userName ? args.userName.toLowerCase() : args.email.split('@')[0],
        password,
        email: args.email,
        group: { connect: { name: 'Pending' } },
        firstName: args.firstName || null,
        lastName: args.lastName || null
      });

      const mailContent = await mailTemplate(user.userName, jwt.sign({ id: user.id }, privateKey, { algorithm: 'RS256', expiresIn: '1d' }));
      const verifyMail = new Mail(user.email, mailContent.subject, mailContent.text, mailContent.html);
      verifyMail.send();

      return {
        token: jwt.sign({ id: user.id }, privateKey, { algorithm: 'RS256', expiresIn: '7d' }),
        id: user.id,
        userName: user.userName,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName
      };
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async loginUser(_, { email, password }, context: Context): Promise<LoginPayload> {
    try {
      const user = await context.prisma.user({ email });

      if (!user) { throw new Error('#ERR_U001'); }

      const vaild = await bcrypt.compare(password, user.password);

      if (!vaild) { throw new Error('#ERR_U00F'); }

      const group = await context.prisma.user({ email }).group();

      if (group.name === 'Banned') { throw new Error('#ERR_UFFF'); }

      return {
        token: jwt.sign({ id: user.id }, privateKey, { algorithm: 'RS256', expiresIn: '7d' }),
        id: user.id,
        userName: user.userName,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        group,
        meta: user.meta
      };
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async verifyUser(_, { token }, context: Context): Promise<LoginPayload> {
    try {
      const tokenPayload = await tokenParser(context, token);

      const user = tokenPayload.user;

      return {
        token: jwt.sign({ id: user.id }, privateKey, { algorithm: 'RS256', expiresIn: '7d' }),
        id: user.id,
        userName: user.userName,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        group: tokenPayload.group,
        meta: user.meta
      };
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async verifyUserEmail(_, { token }, context: Context): Promise<LoginPayload> {
    try {
      const tokenPayload = await tokenParser(context, shorter.decompress(Buffer.from(token, 'base64')));

      const user = await context.prisma.updateUser({
        data: {
          group: { connect: { name: 'Normal' } }
        },
        where: {
          id: tokenPayload.user.id
        }
      });

      return {
        token: jwt.sign({ id: user.id }, privateKey, { algorithm: 'RS256', expiresIn: '7d' }),
        id: user.id,
        userName: user.userName,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        group: tokenPayload.group,
        meta: user.meta
      };
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default userMutation;
