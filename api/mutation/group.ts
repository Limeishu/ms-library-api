import { Context } from 'graphql-yoga/dist/types';
import { PermissionClass, GeneralPayload } from '../types';
import tokenParser from '../util/tokenParser';
import { Group } from '../model';

const Permission = (rule: PermissionClass) => rule;

const groupMutation = {
  async createGroup(_, args, context: Context): Promise<Group> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.group.write) { throw new Error('#ERR_F000'); }

      const groupExist = await context.prisma.group({ name: args.name });

      if (groupExist) { throw new Error('#ERR_G000'); }

      return context.prisma.createGroup({
        name: args.name,
        description: args.description,
        accessbility: Permission(args.accessbility)
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async addUserToGroup(_, { id, groupName }, context: Context): Promise<GeneralPayload> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.group.write) { throw new Error('#ERR_F000'); }

      const targetGroup = await context.prisma.group({ name: groupName });

      if (!targetGroup) { throw new Error('#ERR_G001'); }

      const targetUser = await context.prisma.user({ id });

      if (!targetUser) { throw new Error('#ERR_U001'); }

      await context.prisma.updateUser({
        data: {
          group: {
            connect: {
              name: groupName
            }
          }
        },
        where: {
          id
        }
      });

      return {
        status: 'ok'
      };

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default groupMutation;
