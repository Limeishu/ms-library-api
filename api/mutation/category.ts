import { Context } from 'graphql-yoga/dist/types';
import tokenParser from '../util/tokenParser';
import { Category } from '../model';
import { GeneralPayload, ParagraphPayload } from '../types';

const categoryMutation = {
  async createCategory(_, args, context: Context): Promise<Category> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.category.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.createCategory({
        name: args.name,
        description: args.description,
        content: args.content,
        paragraphs: paragraphPayload
      });
    } catch (err) {
      if (/unique/i.test(err.message)) { throw new Error('#ERR_C000'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateCategory(_, args, context: Context): Promise<Category> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.category.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.updateCategory({
        data: {
          name: args.name,
          description: args.description,
          content: args.content,
          paragraphs: paragraphPayload,
          meta: args.meta
        },
        where: {
          id: args.id
        }
      });
    } catch (err) {
      if (/No Node for the model/i.test(err.message)) { throw new Error('#ERR_Q001'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deleteCategory(_, { id }, context: Context): Promise<GeneralPayload> {
    try {
      const categoryAuthor = await context.prisma.category({ id }).author();

      if (!categoryAuthor) { throw new Error('#ERR_C001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.category.delete || tokenPayload.user.id !== categoryAuthor.id) { throw new Error('#ERR_F000'); }

      await context.prisma.deleteCategory({ id });

      return {
        status: 'ok'
      };

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default categoryMutation;
