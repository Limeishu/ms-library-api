import { Context } from 'graphql-yoga/dist/types';
import { RoutePage, Category, Tag } from '../model/index';
import tokenParser from '../util/tokenParser';
import { GeneralPayload } from '../types';

const routePageMutation = {
  async createRoutePage(_, args, context: Context): Promise<RoutePage> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      let tagsExist: Tag[];
      let tagsExistString: string[];
      let tagsNotNeedToCreate: string[];
      let tagsNeedToCreate: string[];

      let categoriesExist: Category[];
      let categoriesExistString: string[];
      let categoriesNotNeedToCreate: string[];
      let categoriesNeedToCreate: string[];

      if (args.tags) {
        tagsExist = await context.prisma.tags();
        tagsExistString = tagsExist.map(tag => tag.name);
        tagsNotNeedToCreate = args.tags.filter(tag => tagsExistString.indexOf(tag) > -1);
        tagsNeedToCreate = args.tags.filter(tag => tagsExistString.indexOf(tag) === -1);
      }

      if (args.categories) {
        categoriesExist = await context.prisma.categories();
        categoriesExistString = categoriesExist.map(category => category.name);
        categoriesNotNeedToCreate = args.categories.filter(category => categoriesExistString.indexOf(category) > -1);
        categoriesNeedToCreate = args.categories.filter(category => categoriesExistString.indexOf(category) === -1);
      }

      return await context.prisma.createRoutePage({
        description: args.description,
        head: args.head,
        layout: { connect: { name: args.layout } },
        name: args.name,
        path: args.path,
        meta: args.meta,
        targetTags: args.tags ? {
          connect: tagsNotNeedToCreate.map(ele => ({name: ele})),
          create: tagsNeedToCreate.map(ele => ({name: ele}))
        } : null,
        targetCategories: args.categories ? {
          connect: categoriesNotNeedToCreate.map(ele => ({name: ele})),
          create: categoriesNeedToCreate.map(ele => ({name: ele}))
        } : null
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateRoutePage(_, args, context: Context): Promise<RoutePage> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      let tagsExist: Tag[];
      let tagsExistString: string[];
      let tagsNotNeedToCreate: string[];
      let tagsNeedToCreate: string[];
      let tagsNeedToRemove: string[];

      let categoriesExist: Category[];
      let categoriesExistString: string[];
      let categoriesNotNeedToCreate: string[];
      let categoriesNeedToCreate: string[];
      let categoriesNeedToRemove: string[];

      if (args.tags) {
        tagsExist = await context.prisma.tags();
        tagsExistString = tagsExist.map(tag => tag.name);
        tagsNotNeedToCreate = args.tags.filter(tag => tagsExistString.indexOf(tag) > -1);
        tagsNeedToCreate = args.tags.filter(tag => tagsExistString.indexOf(tag) === -1);
        tagsNeedToRemove = args.tags.filter(tag => args.tags.indexOf(tag) === -1);
      }

      if (args.categories) {
        categoriesExist = await context.prisma.categories();
        categoriesExistString = categoriesExist.map(category => category.name);
        categoriesNotNeedToCreate = args.categories.filter(category => categoriesExistString.indexOf(category) > -1);
        categoriesNeedToCreate = args.categories.filter(category => categoriesExistString.indexOf(category) === -1);
        categoriesNeedToRemove = args.categories.filter(category => args.categories.indexOf(category) === -1);
      }

      return await context.prisma.updateRoutePage({
        data: {
          description: args.description,
          head: args.head,
          layout: { connect: { name: args.layout } },
          name: args.name,
          path: args.path,
          meta: args.meta,
          targetTags: args.tags ? {
            connect: tagsNotNeedToCreate.map(ele => ({name: ele})),
            disconnect: tagsNotNeedToCreate.map(ele => ({name: ele})),
            create: tagsNeedToCreate.map(ele => ({name: ele}))
          } : null,
          targetCategories: args.categories ? {
            connect: categoriesNotNeedToCreate.map(ele => ({name: ele})),
            disconnect: categoriesNotNeedToCreate.map(ele => ({name: ele})),
            create: categoriesNeedToCreate.map(ele => ({name: ele}))
          } : null
        },
        where: {
          name: args.name
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deleteRoutePage(_, { name }, context: Context): Promise<GeneralPayload> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      await context.prisma.deleteRoutePage({ name });

      return {
        status: 'ok'
      };
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default routePageMutation;
