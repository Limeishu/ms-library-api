import { Context } from 'graphql-yoga/dist/types';
import { Tag } from '../model';

const tagMutation = {
  async createTag(_, args, context: Context): Promise<Tag> {
    try {
      return await context.prisma.createTag({
        name: args.name,
        description: args.description,
        meta: args.meta
      });
    } catch (err) {
      if (/unique/i.test(err.message)) { throw new Error('#ERR_T000'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateTag(_, args, context: Context): Promise<Tag> {
    try {
      return await context.prisma.updateTag({
        data: {
          name: args.name,
          description: args.description,
          meta: args.meta
        },
        where: {
          id: args.id
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default tagMutation;
