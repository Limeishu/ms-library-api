import { Context } from 'graphql-yoga/dist/types';
import { Layout } from '../model/index';
import tokenParser from '../util/tokenParser';

const layoutMutation = {
  async createLayout(_, args, context: Context): Promise<Layout> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      return context.prisma.createLayout({
        description: args.description,
        name: args.name,
        component: args.component
      });
    } catch (err) {
      if (/unique/i.test(err.message)) { throw new Error('#ERR_L000'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateLayout(_, args, context: Context): Promise<Layout> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      return context.prisma.updateLayout({
        data: {
          description: args.description,
          component: args.component
        },
        where: {
          name: args.name
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deleteLayout(_, { name }, context: Context): Promise<Layout> {
    try {
      const tokenPayload = await tokenParser(context);

      if (tokenPayload.group.name !== 'Admin') { throw new Error('#ERR_F000'); }

      return context.prisma.createLayout({ name });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default layoutMutation;
