import categoryMutation from './category';
import creatorMutation from './creator';
import groupMutation from './group';
import imageMutation from './image';
import itemMutation from './item';
import layoutMutation from './layout';
import postMutation from './post';
import routePageMutation from './routePage';
import tagMutation from './tag';
import userMutation from './user';

const Mutation = {
  ...categoryMutation,
  ...creatorMutation,
  ...groupMutation,
  ...imageMutation,
  ...itemMutation,
  ...layoutMutation,
  ...postMutation,
  ...routePageMutation,
  ...tagMutation,
  ...userMutation
};

export default Mutation;
