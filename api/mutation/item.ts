import { Context } from 'graphql-yoga/dist/types';
import tokenParser from '../util/tokenParser';
import { Category, Creator, Item } from '../model';
import { GeneralPayload, ParagraphPayload } from '../types';

const itemMutation = {
  async createItem(_, args, context: Context): Promise<Item> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.item.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.createItem({
        name: args.name,
        description: args.description,
        content: args.content,
        paragraphs: paragraphPayload,
        date: args.date
      });
    } catch (err) {
      if (/unique/i.test(err.message)) { throw new Error('#ERR_I000'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async updateItem(_, args, context: Context): Promise<Item> {
    try {
      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.item.write) { throw new Error('#ERR_F000'); }

      const paragraphPayload: ParagraphPayload = args.paragraphs;

      return await context.prisma.updateItem({
        data: {
          name: args.name,
          description: args.description,
          content: args.content,
          paragraphs: paragraphPayload,
          creator: { connect: { name: args.creatorName } },
          date: args.date,
          meta: args.meta
        },
        where: {
          id: args.id
        }
      });
    } catch (err) {
      if (/No Node for the model/i.test(err.message)) { throw new Error('#ERR_Q001'); }
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async setCategoryToItem(_, { id, categories }, context: Context): Promise<Item> {
    try {
      const targetItem = await context.prisma.item({ id });

      if (!targetItem) { throw new Error('#ERR_I001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.item.write) { throw new Error('#ERR_F000'); }

      const itemExCategory = await context.prisma.item({ id }).categories();
      const itemExCategoryString: string[] = itemExCategory.map(ele => ele.name);

      const categoriesExist: Category[] = await context.prisma.categories();
      const categoriesExistString: string[] = categoriesExist.map(tag => tag.name);
      const categoriesNotNeedToCreate: string[] = categories.filter(ele => categoriesExistString.indexOf(ele) > -1);
      const categoriesNeedToCreate: string[] = categories.filter(ele => categoriesExistString.indexOf(ele) === -1);
      const categoriesNeedToRemove: string[] = itemExCategoryString.filter(ele => categories.indexOf(ele) === -1);

      return await context.prisma.updateItem({
        data: {
          categories: {
            connect: categoriesNotNeedToCreate.map(ele => ({name: ele})),
            disconnect: categoriesNeedToRemove.map(ele => ({name: ele})),
            create: categoriesNeedToCreate.map(ele => ({name: ele}))
          }
        },
        where: {
          id
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async setCreatorToItem(_, { id, creators }, context: Context): Promise<Item> {
    try {
      const targetItem = await context.prisma.item({ id });

      if (!targetItem) { throw new Error('#ERR_I001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.item.write) { throw new Error('#ERR_F000'); }

      const itemExCreator = await context.prisma.item({ id }).creators();
      const itemExCreatorString: string[] = itemExCreator.map(ele => ele.name);

      const creatorsExist: Creator[] = await context.prisma.creators();
      const creatorsExistString: string[] = creatorsExist.map(tag => tag.name);
      const creatorsNotNeedToCreate: string[] = creators.filter(ele => creatorsExistString.indexOf(ele) > -1);
      const creatorsNeedToCreate: string[] = creators.filter(ele => creatorsExistString.indexOf(ele) === -1);
      const creatorsNeedToRemove: string[] = itemExCreatorString.filter(ele => creators.indexOf(ele) === -1);

      return await context.prisma.updateItem({
        data: {
          creators: {
            connect: creatorsNotNeedToCreate.map(ele => ({name: ele})),
            disconnect: creatorsNeedToRemove.map(ele => ({name: ele})),
            create: creatorsNeedToCreate.map(ele => ({name: ele}))
          }
        },
        where: {
          id
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async deleteItem(_, { id }, context: Context): Promise<GeneralPayload> {
    try {
      const itemAuthor = await context.prisma.item({ id }).author();

      if (!itemAuthor) { throw new Error('#ERR_I001'); }

      const tokenPayload = await tokenParser(context);

      if (!tokenPayload.group.accessbility.item.delete || tokenPayload.user.id !== itemAuthor.id) { throw new Error('#ERR_F000'); }

      await context.prisma.deleteItem({ id });

      return {
        status: 'ok'
      };

    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default itemMutation;
