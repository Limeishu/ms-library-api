import { prisma } from './model';
import { IResolvers } from 'graphql-middleware/dist/types';
import Query from './query';
import Mutation from './mutation';
import Resolver from './resolver';

const resolvers: IResolvers = {
  Query,
  Mutation,
  ...Resolver
};

export {
  resolvers,
  prisma
};
