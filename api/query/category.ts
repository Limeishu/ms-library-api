import { Context } from 'graphql-yoga/dist/types';
import { Category } from '../model/index';

const categoryQuery = {
  async category(_, { id }, context: Context): Promise<Category> {
    try {
      const targetCategory = await context.prisma.category({ id });

      if (!targetCategory) { throw new Error('#ERR_I001'); }

      return targetCategory;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async categories(_, args, context: Context): Promise<Category[]> {
    try {
      return await context.prisma.categories();
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default categoryQuery;
