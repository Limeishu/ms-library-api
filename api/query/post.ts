import { Context } from 'graphql-yoga/dist/types';
import { Post } from '../model/index';

const postQuery = {
  async post(_, { id }, context: Context): Promise<Post> {
    try {
      const targetPost = await context.prisma.post({ id });

      if (!targetPost) { throw new Error('#ERR_P001'); }

      return targetPost;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async posts(_, args, context: Context): Promise<Post[]> {
    try {
      const posts = await context.prisma.posts({
        where: {
          published: true
        }
      });

      return posts;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async postsByUser(_, { userName }, context: Context): Promise<Post> {
    try {
      const targetUser = await context.prisma.user({ userName });

      if (!targetUser) { throw new Error('#ERR_U001'); }

      return await context.prisma.posts({
        where: {
          published: true,
          author: { userName }
        }
      });
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default postQuery;
