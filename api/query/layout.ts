import { Context } from 'graphql-yoga/dist/types';
import { Layout } from '../model/index';

const layoutQuery = {
  async layout(_, { name }, context: Context): Promise<Layout> {
    try {
      const targetLayout = await context.prisma.layout({ name });

      if (!targetLayout) { throw new Error('#ERR_L001'); }

      return targetLayout;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async layouts(_, args, context: Context): Promise<Layout[]> {
    try {
      const targetLayout = await context.prisma.layouts();

      return targetLayout;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default layoutQuery;
