import { Context } from 'graphql-yoga/dist/types';
import { Item } from '../model/index';

const itemQuery = {
  async item(_, { id }, context: Context): Promise<Item> {
    try {
      const targetItem = await context.prisma.item({ id });

      if (!targetItem) { throw new Error('#ERR_I001'); }

      return targetItem;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async items(_, args, context: Context): Promise<Item[]> {
    try {
      return await context.prisma.items();
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default itemQuery;
