import { Context } from 'graphql-yoga/dist/types';
import { RoutePage } from '../model/index';

const routePageQuery = {
  async route(_, { name }, context: Context): Promise<RoutePage> {
    try {
      const targetRoute = await context.prisma.routePage({ name });

      if (!targetRoute) { throw new Error('#ERR_H404'); }

      return targetRoute;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async routes(_, args, context: Context): Promise<RoutePage> {
    try {
      const targetRoute = await context.prisma.routePages();

      if (!targetRoute) { throw new Error('#ERR_H404'); }

      return targetRoute;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default routePageQuery;
