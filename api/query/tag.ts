import { Context } from 'graphql-yoga/dist/types';
import { Tag } from '../model/index';

const tagQuery = {
  async tag(_, { id }, context: Context): Promise<Tag> {
    try {
      const targetTag = await context.prisma.tag({ id });

      if (!targetTag) { throw new Error('#ERR_I001'); }

      return targetTag;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async tags(_, args, context: Context): Promise<Tag[]> {
    try {
      return await context.prisma.tags();
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default tagQuery;
