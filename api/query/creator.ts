import { Context } from 'graphql-yoga/dist/types';
import { Creator } from '../model/index';

const creatorQuery = {
  async creator(_, { id }, context: Context): Promise<Creator> {
    try {
      const targetCreator = await context.prisma.creator({ id });

      if (!targetCreator) { throw new Error('#ERR_I001'); }

      return targetCreator;
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  },

  async creators(_, args, context: Context): Promise<Creator[]> {
    try {
      return await context.prisma.creators();
    } catch (err) {
      throw new Error(err.message || '#ERR_FFFF');
    }
  }
};

export default creatorQuery;
