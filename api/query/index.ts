import categoryQuery from './category';
import creatorQuery from './creator';
import itemQuery from './item';
import layout from './layout';
import postQuery from './post';
import routePage from './routePage';
import tagQuery from './tag';

const Query = {
  ...categoryQuery,
  ...creatorQuery,
  ...itemQuery,
  ...layout,
  ...postQuery,
  ...routePage,
  ...tagQuery
};

export default Query;
