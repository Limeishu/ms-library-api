scalar DateTime
scalar Json
scalar Upload
scalar UUID

# Query

type Query {
  creator(id: UUID!): Creator
  creators: [Creator!]

  category(id: ID!): Category
  categories: [Category!]

  item(id: ID!): Item
  items: [Item!]

  layout(name: String!): Layout
  layouts: [Layout!]

  post(id: ID!): Post
  posts: [Post!]
  postsByUser(userName: String!): [Post!]

  route(name: String!): RoutePage
  routes: [RoutePage!]

  tag(id: ID!): Tag
  tags: [Tag!]
}

# Mutation

type Mutation {
  verifyUser(token: String!): UserPayload!
  verifyUserEmail(token: String!): UserPayload!
  signUpUser(userName: String, password: String!, email: String!, firstName: String, lastName: String): UserPayload!
  loginUser(email: String!, password: String!): LoginPayload!

  createGroup(name: String!, description: String, accessbility: Json!): Group!
  addUserToGroup(id: UUID!, groupName: String!): GeneralPayload!

  createPost(title: String!, content: String, paragraphs: Json): Post!
  updatePost(id: ID!, title: String, content: String, paragraphs: Json, published: Boolean, meta: Json): Post!
  setTagsToPost(id: ID!, tags: [String!]): Post!
  deletePost(id: ID!): GeneralPayload!

  createItem(name: String!, description: String, content: String, paragraphs: Json, date: DateTime): Item!
  updateItem(id: UUID!, name: String, description: String, content: String, paragraphs: Json, creatorName: String, date: DateTime, meta: Json): Item!
  setCategoryToItem(id: UUID!, categories: [String!]!): Item!
  setCreatorToItem(id: UUID!, creators: [String!]!): Item!
  deleteItem(id: UUID!): GeneralPayload!

  createCreator(name: String!, description: String, content: String, paragraphs: Json): Creator!
  updateCreator(id: UUID!, name: String, description: String, content: String, paragraphs: Json, meta: Json): Creator!
  deleteCreator(id: UUID!): GeneralPayload!

  createCategory(name: String!, description: String, content: String, paragraphs: Json): Category!
  updateCategory(id: UUID!, name: String, description: String, content: String, paragraphs: Json, meta: Json): Category!
  deleteCategory(id: UUID!): GeneralPayload!

  createTag(name: String!, description: String, meta: Json): Tag!
  updateTag(id: ID!, name: String, description: String, meta: Json): Tag!
  deleteTag(id: ID!): GeneralPayload!

  createRoutePage(name: String!, description: String, head: Json, layout: String, path: String!, meta: Json, tags: [String!], categories: [String!]): RoutePage!
  updateRoutePage(name: String!, description: String, head: Json, layout: String, path: String!, meta: Json, tags: [String!], categories: [String!]): RoutePage!
  deleteRoutePage(name: String!): GeneralPayload!

  createLayout(name: String!, component: String! , description: String, meta: Json): Layout!
  updateLayout(name: String!, component: String! , description: String, meta: Json): Layout!
  deleteLayout(name: String!): GeneralPayload!

  uploadImage(file: Upload!): Image
}

# Payload

type AuthorPayload {
  id: UUID!
  email: String
  userName: String
  firstName: String
  lastName: String
  meta: Json
}

type GeneralPayload {
  status: String
  message: String
  meta: Json
}

type LoginPayload {
  id: UUID!
  token: String!
  userName: String
  firstName: String
  lastName: String
  email: String
  group: Group
  meta: Json
}

type PermissionPayload {
  read: Boolean
  write: Boolean
  delete: Boolean
}

type PermissionClass {
  category: PermissionPayload
  commit: PermissionPayload
  creator: PermissionPayload
  item: PermissionPayload
  group: PermissionPayload
  post: PermissionPayload
  user: PermissionPayload
}

type UserPayload {
  id: UUID!
  token: String!
  userName: String
  firstName: String
  lastName: String
  email: String
  meta: Json
}

# Schema

type Category {
  id: UUID!
# ---
  content: String
  description: String
  name: String!
  meta: Json
# ---
  items: [Item!]
  paragraphs: [Paragraph!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Creator {
  id: UUID!
# ---
  content: String
  description: String
  name: String!
  meta: Json
# ---
  items: [Item!]
  paragraphs: [Paragraph!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Group {
  id: ID!
# ---
  accessbility: PermissionClass!
  description: String
  name: String!
  meta: Json
# ---
  users: [User!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Image {
  id: UUID!
# ---
  description: String
  name: String
  path: String!
  meta: Json
# ---
  user: UserPayload!
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Item {
  id: UUID!
# ---
  content: String
  date: DateTime
  description: String
  name: String!
  meta: Json
# ---
  categories: [Category!]
  creators: [Creator!]
  paragraphs: [Paragraph!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Layout {
  id: ID!
# ---
  description: String
  name: String!
  component: String!
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Note {
  id: ID!
# ---
  content: String!
  meta: Json
# ---
  notes: [Note!]
  targetQuote: Quote!
  user: User!
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Paragraph {
  id: ID!
# ---
  index: Int
  markup: [Json!]
  text: String
  type: String!
  meta: Json
# ---
  quotes: [Quote!]
  responses: [Response!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Post {
  id: ID!
# ---
  author: AuthorPayload
  content: String
  title: String!
  published: Boolean!
  meta: Json
# ---
  paragraphs: [Paragraph!]
  responses: [Response!]
  tags: [Tag!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Quote {
  id: ID!
# ---
  startOffset: Int!
  endOffset: Int!
  type: String!
  meta: Json
# ---
  notes: [Note!]
  targetParagraph: Paragraph!
  user: User!
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Response {
  id: ID!
# ---
  content: String!
  meta: Json
# ---
  user: User!
  responses: [Response!]
  targetPost: Post!
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type RoutePage {
  id: ID!
# ---
  description: String
  head: Json
  layout: Layout!
  name: String!
  path: String!
  meta: Json
# ---
  targetTags: [Tag!]
  targetCategories: [Category!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type Tag {
  id: ID!
# ---
  description: String
  name: String!
  meta: Json
# ---
  posts: [Post!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}

type User {
  id: UUID!
# ---
  email: String!
  firstName: String
  group: Group!
  lastName: String
  password: String!
  userName: String!
  meta: Json
# ---
  notes: [Note!]
  posts: [Post!]
  quotes: [Quote!]
  responses: [Response!]
# ---
  createdAt: DateTime!
  updatedAt: DateTime!
}
